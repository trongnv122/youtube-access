#!/bin/bash

./mvnw package
tar -C ./target -cvf quarkus-app.tar quarkus-app
rsync -avzh quarkus-app.tar fami:/home/ubuntu/app/
ssh fami /home/ubuntu/app/run-fami.sh
