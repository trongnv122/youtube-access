CREATE DATABASE youtube_access CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'myuser'@'localhost' IDENTIFIED BY 'somepass';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX,
    ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON youtube_access.* TO 'myuser'@'localhost';

FLUSH PRIVILEGES;
