package net.bestinme.fami.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ActiveRequest {
    @NotBlank
    private String hash;
}
