package net.bestinme.fami.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
public class RegisterRequest {
    @NotBlank(message = "Tên không được để trống.")
    private String fullName;
    @NotBlank(message = "Mật khẩu không được để trống.")
    private String password;
    @NotBlank(message = "Email không được để trống.")
    private String email;
    @NotBlank(message = "Số điện thoại không được để trống.")
    private String phone;
    private LocalDate dob;
    private String gender;
    @NotBlank(message = "Tên đăng nhập không được để trống.")
    private String username;
}
