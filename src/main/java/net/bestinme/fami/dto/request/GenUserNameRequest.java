package net.bestinme.fami.dto.request;

import lombok.Data;


@Data
public class GenUserNameRequest {
    private String firstName;
    private String lastName;
}
