package net.bestinme.fami.dto.user;

import java.util.Set;

public class UserResponse {
    public Long id;
    public String firstName;
    public String lastName;
    public String email;
    public Set<String> roles;
    public String sbd;
    public String gender;
    public String avatar;
    public int classLevel;

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
