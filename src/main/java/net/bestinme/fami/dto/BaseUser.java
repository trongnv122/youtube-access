package net.bestinme.fami.dto;

import lombok.NoArgsConstructor;
import net.bestinme.fami.dto.user.UserResponse;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor
public class BaseUser {
    public BaseUser(UserResponse userResponse) {
        this.id = userResponse.id;
        this.name = Stream.of(userResponse.firstName, userResponse.lastName)
                .filter(s -> s != null && !s.isEmpty()).collect(Collectors.joining(" "));
    }

    public Long id;
    public String name;
}
