package net.bestinme.fami.dto;

import java.util.List;

public class ListResponse<T> {
    public ListResponse(List<T> items, Long totalRecord) {
        this.totalRecord = totalRecord;
        this.items = items;
    }

    public Long totalRecord;
    public List<T> items;
}
