package net.bestinme.fami.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.bestinme.fami.dto.user.UserResponse;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class AuthResponse {
    public String token;
    public UserResponse user;
}
