package net.bestinme.fami.dto;


import net.bestinme.fami.common.Sort;

import java.util.HashMap;
import java.util.Map;


public class ListRequest {
    public int page = 0;
    public int size = 25;
    public Map<String, Object> query = new HashMap<>();
    public Sort sort = new Sort();

    public int getLimit() {
        return size;
    }
    public int getOffset() {
        return page * size;
    }
}
