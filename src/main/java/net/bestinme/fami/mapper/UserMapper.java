package net.bestinme.fami.mapper;

import net.bestinme.fami.dto.request.RegisterRequest;
import net.bestinme.fami.dto.user.UserResponse;
import net.bestinme.fami.entity.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface UserMapper {
    UserResponse toUserResponse(UserEntity user);

    UserEntity toUser(RegisterRequest request);
}
