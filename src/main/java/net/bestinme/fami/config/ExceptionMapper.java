package net.bestinme.fami.config;

import net.bestinme.fami.common.ErrorInfo;
import net.bestinme.fami.common.FamException;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.Collection;
import java.util.Optional;

@Provider
public class ExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Throwable> {
    private static final Logger LOG = Logger.getLogger(ExceptionMapper.class);

    @ServerExceptionMapper(ConstraintViolationException.class)
    public Response handleConstraintViolationException(ConstraintViolationException e) {
        LOG.error("ConstraintViolationException", e);
        final ErrorInfo errorInfo = ErrorInfo.BAD_REQUEST;
        Optional.ofNullable(e.getConstraintViolations()).stream()
                .flatMap(Collection::stream).forEach(i -> {
                    errorInfo.getMessageDetails().put(i.getPropertyPath().toString(), i.getMessage());
                });
        return Response.status(Response.Status.BAD_REQUEST).
                entity(errorInfo).build();
    }

    @Override
    public Response toResponse(Throwable e) {
        LOG.error("Error occur", e);
        if (e instanceof FamException) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                    entity(((FamException) e).getError()).build();
        } else {

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                    entity(ErrorInfo.UNKNOWN_ERROR).build();
        }
    }
}
