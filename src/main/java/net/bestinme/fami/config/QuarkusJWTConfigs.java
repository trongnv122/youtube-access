package net.bestinme.fami.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

/**
 * NOTES: with prefix is EMPTY, Interface Mapping require all of available properties.
 * So this will cause error if missing get method for all.
 * Should separate configs with specific prefix
 */

@ConfigMapping(prefix = "quarkusjwt", namingStrategy = ConfigMapping.NamingStrategy.SNAKE_CASE)
public interface QuarkusJWTConfigs {
    @WithName("verify.issuer")
    String getVerifyIssuer();

    @WithName("jwt.duration")
    Long getJwtDuration();

    @WithName("password.secret")
    String getPasswordSecret();

    @WithName("password.iteration")
    Integer getPasswordIteration();

    @WithName("password.keylength")
    Integer getPasswordKeyLength();

    @WithName("sign.key.location")
    String getSignKeyLocation();

}
