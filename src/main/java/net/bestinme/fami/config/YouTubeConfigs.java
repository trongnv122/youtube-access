package net.bestinme.fami.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

@ConfigMapping(prefix = "youtube", namingStrategy = ConfigMapping.NamingStrategy.SNAKE_CASE)
public interface YouTubeConfigs {
    @WithName("apikey")
    String getApikey();
}
