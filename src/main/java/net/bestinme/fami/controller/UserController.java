package net.bestinme.fami.controller;

import lombok.AllArgsConstructor;
import net.bestinme.fami.dto.AuthRequest;
import net.bestinme.fami.dto.request.ActiveRequest;
import net.bestinme.fami.dto.request.GenUserNameRequest;
import net.bestinme.fami.dto.request.RegisterRequest;
import net.bestinme.fami.dto.user.UserResponse;
import net.bestinme.fami.service.UserService;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@AllArgsConstructor(onConstructor = @__(@Inject))
public class UserController {
    JsonWebToken jwt;

    UserService userService;

    @PermitAll
    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(AuthRequest authRequest) throws Exception {
        return Response.ok(userService.authenticate(authRequest)).build();
    }

    @GET
    @Path("me")
    @RolesAllowed("User")
    @Produces(MediaType.APPLICATION_JSON)
    public UserResponse me() {
        return userService.getMe();
    }


    @POST
    @Path("register")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean register(RegisterRequest request) {
        userService.register(request);
        return Boolean.TRUE;
    }

    @POST
    @Path("active")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean active(ActiveRequest request) {
        userService.active(request);
        return Boolean.TRUE;
    }

    @POST
    @Path("/username-generate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    public String generateUsername(GenUserNameRequest request) {
        return userService.generateUsername(request);
    }
}
