package net.bestinme.fami.entity;


public class UserRegistrationEntity extends BaseEntity {
    public UserRegistrationEntity(Long userId, String key, String value) {
        this.userId = userId;
        this.key = key;
        this.value = value;
    }

    public Long id;
    public Long userId;
    public String key;
    public String value;
    public Integer status;
}
