package net.bestinme.fami.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;

public class UserEntity extends BaseEntity {

    public Long id;
    public String firstName;
    public String lastName;
    public String password;
    public String email;
    public String phone;
    public String username;
    public LocalDate dob;
    public Integer status;
    public String sbd;
    public String gender;
    public Long parentId;
    public String avatar;
    public int classLevel;

    @JsonIgnore
    public String getIdString() {
        return id.toString();
    }
}
