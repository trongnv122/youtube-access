package net.bestinme.fami.entity;


import java.util.Date;

public class BaseEntity {
    public Long id;
    public Date createdAt;
    public Date updatedAt;
    public Long createdBy;
    public Long updatedBy;
}
