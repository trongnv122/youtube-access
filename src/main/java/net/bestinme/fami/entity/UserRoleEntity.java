package net.bestinme.fami.entity;


public class UserRoleEntity {
    public Long userId;
    public int roleId;
    public String roleName;

    public UserRoleEntity() {
    }

    public UserRoleEntity(Long userId, int roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

    public UserRoleEntity(Long userId, int roleId, String roleName) {
        this.userId = userId;
        this.roleId = roleId;
        this.roleName = roleName;
    }
}
