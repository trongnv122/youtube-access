package net.bestinme.fami.repository;

import net.bestinme.fami.common.Role;
import net.bestinme.fami.entity.UserEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Set;

@Mapper
public interface UserRepository {
    String BASE_COLUMNS = " id, firstName, lastName, password, email, phone, username, dob, status," +
            " gender, avatar, sbd, classLevel, createdAt, updatedAt, createdBy, updatedBy, parentId ";
    String BASE_SELECT = " SELECT " + BASE_COLUMNS + " FROM `user` ";

    @Select(BASE_SELECT + " WHERE phone = #{phone};")
    UserEntity findByPhone(String phone);

    @Select(BASE_SELECT + " WHERE UPPER(username) like UPPER(#{username});")
    UserEntity findByUsername(String username);

    @Select(BASE_SELECT + " WHERE id = #{id};")
    UserEntity findById(long id);

    @Select(BASE_SELECT + " WHERE UPPER(email) like UPPER(#{email})")
    UserEntity findByEmail(String email);

    @Select(BASE_SELECT + " WHERE UPPER(username) rlike '^${username}[0-9]+$' ORDER BY username LIMIT #{limit} OFFSET #{offset} ")
    List<UserEntity> findUsernameLike(String username, int limit, long offset);

    @Delete("DELETE FROM user WHERE id = #{id}")
    void deleteById(long id);

    @Select("INSERT INTO user(firstName, lastName, password, email, phone, username, dob, status," +
            "gender, avatar, sbd, classLevel, createdBy, parentId) " +
            " VALUES (#{firstName}, #{lastName}, #{password}, #{email}, #{phone}, #{username}, #{dob}, " +
            "#{status}, #{gender}, #{avatar}, #{sbd}, #{classLevel}, #{createdBy}, #{parentId}) RETURNING id;")
    Long insert(UserEntity user);

    @Update("Update user set firstName=#{firstName}, " +
            " lastName=#{lastName}, email=#{email} , phone=#{phone} where id=#{id}")
    void update(UserEntity user);

    @Update("Update user set status=#{status} " +
            "WHERE id=#{id}")
    void updateStatus(UserEntity user);

    @Select("SELECT name FROM role r inner join user_role ur ON ur.roleId = r.id WHERE ur.userId = #{userId}")
    Set<Role> getRole(Long userId);

    @Insert("INSERT INTO user_role(userId, roleId) " +
            " VALUES (#{userId}, #{roleId});")
    void insertRole(Long userId, Integer roleId);

    @Update("Update user SET parentId=#{parentId} where id=#{userId};")
    void updateParent(Long userId, Long parentId);


}
