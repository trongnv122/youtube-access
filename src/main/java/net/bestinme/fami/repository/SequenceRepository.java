package net.bestinme.fami.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


@Mapper
public interface SequenceRepository {
    @Select("INSERT INTO sequence(code, year, seq) " +
            " VALUES (#{code}, #{year}, 1)  ON DUPLICATE KEY UPDATE seq = seq + 1 RETURNING seq ;")
    Long createByCodeAndYear(String code, int year);

    @Insert("INSERT INTO sequence(code, year, seq) " +
            " VALUES (#{code}, 1, 1) ON DUPLICATE KEY UPDATE seq = seq + 1  RETURNING seq;")
    Long createByCode(String code);

}
