package net.bestinme.fami.repository;

import net.bestinme.fami.entity.UserRegistrationEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Collection;
import java.util.List;


@Mapper
public interface UserRegistrationRepository {

    @Select("SELECT * FROM user_registration " +
            "WHERE userId = #{userId} AND `key` = #{key} ORDER BY createdAt DESC LIMIT 1;")
    UserRegistrationEntity findByUserIdAndKey(long userId, String key);

    @Select("SELECT * FROM user_registration " +
            "WHERE `key` = #{key} AND `value` = #{value} ORDER BY createdAt DESC LIMIT 1;")
    UserRegistrationEntity findByKeyAndValue(String key, String value);

    @Select("INSERT user_registration (userId, `key`, `value`) VALUES (#{userId}, #{key}, #{value});")
    void insert(UserRegistrationEntity data);

    @Select("SELECT * FROM user_registration " +
            "WHERE userId = #{userId} AND `key` in #{keys};")
    List<UserRegistrationEntity> findByUserIdAndKeys(long userId, Collection<String> keys);

    @Delete("UPDATE user_registration SET status = #{status} WHERE id = ${id};")
    void updateStatus(long id, Integer status);
}
