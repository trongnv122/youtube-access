package net.bestinme.fami.repository;

import net.bestinme.fami.entity.UserRoleEntity;
import org.apache.ibatis.annotations.*;

import java.util.Collection;
import java.util.List;

@Mapper
public interface UserRoleRepository {
    @Insert({
            "<script>",
            "INSERT INTO user_role(userId, roleId) ",
            " VALUES ",
            "<foreach item='item' index='index' collection='roles'",
            " separator=','>",
            "(${item.userId}, ${item.roleId}) ",
            "</foreach>",
            "</script>"
    })
    Long insert(Collection<UserRoleEntity> roles);

    @Delete("DELETE FROM user_role WHERE  userId = #{userId};")
    void clean(Long userId);

    @Select({
            "<script>",
            "SELECT userId, roleId, r.name as roleName FROM user_role ur INNER JOIN role r ON r.id = ur.roleId ",
            " WHERE  userId = #{userId}",
            "</script>"
    })
    List<UserRoleEntity> list(Long userId);

}
