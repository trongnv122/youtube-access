package net.bestinme.fami.service;

import org.eclipse.microprofile.jwt.JsonWebToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public abstract class BaseService {
    @Inject
    protected JsonWebToken jwt;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected Long getCurrentUserId() {
        return Long.parseLong(jwt.getClaim("uid"));
    }
}
