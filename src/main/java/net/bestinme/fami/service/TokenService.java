package net.bestinme.fami.service;

import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.build.JwtClaimsBuilder;
import net.bestinme.fami.config.QuarkusJWTConfigs;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Set;

/**
 * @author trongnv
 */
@ApplicationScoped
public class TokenService {
    @Inject
    public TokenService(QuarkusJWTConfigs quarkusJWTConfigs) throws Exception {
        this.quarkusJWTConfigs = quarkusJWTConfigs;
        this.privateKey = readPrivateKey(quarkusJWTConfigs.getSignKeyLocation());
    }

    final QuarkusJWTConfigs quarkusJWTConfigs;
    final PrivateKey privateKey;

    public String generateToken(String username, String uid, Set<String> roles, Long duration, String issuer) throws Exception {
        JwtClaimsBuilder claimsBuilder = Jwt.claims();
        long currentTimeInSecs = currentTimeInSecs();

        claimsBuilder.issuer(issuer);
        claimsBuilder.subject(username);
        claimsBuilder.claim("uid", uid);
        claimsBuilder.issuedAt(currentTimeInSecs);
        claimsBuilder.expiresAt(currentTimeInSecs + duration);
        claimsBuilder.groups(roles);

        return claimsBuilder.jws().keyId(quarkusJWTConfigs.getSignKeyLocation()).sign(privateKey);
    }

    public PrivateKey readPrivateKey(final String pemResName) throws Exception {
        File file = new File(pemResName);

        try (InputStream contentIS = new FileInputStream(file)) {
            byte[] tmp = new byte[4096];
            int length = contentIS.read(tmp);
            return decodePrivateKey(new String(tmp, 0, length, StandardCharsets.UTF_8));
        }
    }

    public static PrivateKey decodePrivateKey(final String pemEncoded) throws Exception {
        byte[] encodedBytes = toEncodedBytes(pemEncoded);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);
    }

    public static byte[] toEncodedBytes(final String pemEncoded) {
        final String normalizedPem = removeBeginEnd(pemEncoded);
        return Base64.getDecoder().decode(normalizedPem);
    }

    public static String removeBeginEnd(String pem) {
        pem = pem.replaceAll("-----BEGIN (.*)-----", "");
        pem = pem.replaceAll("-----END (.*)----", "");
        pem = pem.replaceAll("\r\n", "");
        pem = pem.replaceAll("\n", "");
        return pem.trim();
    }

    public static int currentTimeInSecs() {
        long currentTimeMS = System.currentTimeMillis();
        return (int) (currentTimeMS / 1000);
    }

}