package net.bestinme.fami.service;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeRequestInitializer;
import net.bestinme.fami.config.YouTubeConfigs;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;


@ApplicationScoped
public class YouTubeDataAPI {
    YouTube youTube;

    @Inject
    public YouTubeDataAPI(YouTubeConfigs configs) {
        youTube = YouTubeDataAPI.getService(configs);
    }

    public YouTube get() {
        return youTube;
    }

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    public static YouTube getService(YouTubeConfigs configs) {
        return new YouTube.Builder(new NetHttpTransport(), JSON_FACTORY, request -> {
        }).setApplicationName("youtube-access")
                .setYouTubeRequestInitializer
                        (new YouTubeRequestInitializer(configs.getApikey())).build();

    }
}
