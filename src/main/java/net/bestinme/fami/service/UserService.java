package net.bestinme.fami.service;

import io.quarkus.security.AuthenticationFailedException;
import lombok.AllArgsConstructor;
import net.bestinme.fami.common.Assert;
import net.bestinme.fami.common.ErrorInfo;
import net.bestinme.fami.common.FamException;
import net.bestinme.fami.common.Role;
import net.bestinme.fami.config.QuarkusJWTConfigs;
import net.bestinme.fami.dto.AuthRequest;
import net.bestinme.fami.dto.AuthResponse;
import net.bestinme.fami.dto.request.ActiveRequest;
import net.bestinme.fami.dto.request.GenUserNameRequest;
import net.bestinme.fami.dto.request.RegisterRequest;
import net.bestinme.fami.dto.user.UserResponse;
import net.bestinme.fami.entity.UserEntity;
import net.bestinme.fami.entity.UserRegistrationEntity;
import net.bestinme.fami.entity.UserRoleEntity;
import net.bestinme.fami.enums.RegistrationKey;
import net.bestinme.fami.enums.RegistrationStatus;
import net.bestinme.fami.enums.SequenceType;
import net.bestinme.fami.enums.UserStatus;
import net.bestinme.fami.mapper.UserMapper;
import net.bestinme.fami.repository.SequenceRepository;
import net.bestinme.fami.repository.UserRegistrationRepository;
import net.bestinme.fami.repository.UserRepository;
import net.bestinme.fami.repository.UserRoleRepository;
import net.bestinme.fami.utils.PBKDF2Encoder;
import org.apache.commons.lang3.StringUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
@AllArgsConstructor(onConstructor = @__(@Inject))
public class UserService extends BaseService {
    QuarkusJWTConfigs quarkusJWTConfigs;

    TokenService tokenService;

    UserRepository userRepository;
    UserRegistrationRepository registrationRepository;
    SequenceRepository sequenceRepository;
    UserRoleRepository userRoleRepository;

    PBKDF2Encoder passwordEncoder;

    UserMapper userMapper;

    public AuthResponse authenticate(AuthRequest authRequest) throws Exception {
        final UserEntity user = userRepository.findByUsername(authRequest.getUsername());

        if (user == null || !user.password.equals(passwordEncoder.encode(authRequest.getPassword()))) {
            throw new AuthenticationFailedException("Login Failed");
        }

        final Set<String> roles = userRoleRepository.list(user.id).stream().map(i -> i.roleName)
                .collect(Collectors.toSet());

        AuthResponse response = new AuthResponse();
        response.token = tokenService.generateToken(user.username, user.id.toString(),
                roles, quarkusJWTConfigs.getJwtDuration(), quarkusJWTConfigs.getVerifyIssuer());
        response.user = userMapper.toUserResponse(user);

        return response;
    }

    public UserResponse getMe() {
        UserEntity user = userRepository.findById(Long.parseLong(jwt.getClaim("uid")));
        return userMapper.toUserResponse(user);
    }

    @Transactional
    public UserResponse register(@Valid RegisterRequest request) {
        Assert.isTrue(request.getFullName() != null && !request.getFullName().isEmpty(),
                "fullName", "Tên đầy đủ không được trống");

        Assert.isTrue(request.getFullName().split(" ").length >= 2,
                "fullName", "Điền đủ họ và tên");

        String pattern = "^[\\p{L}\\p{M} ]+$";
        Assert.isTrue(request.getFullName().matches(pattern),
                "fullName", "Tên chỉ bao gồm chữ và khoảng trắng");

        UserEntity user = userRepository.findByUsername(request.getUsername());
        if (user != null) {
            throw new FamException(ErrorInfo.USER_EXISTED);
        }

        UserEntity userEntity = userMapper.toUser(request);

        String[] nameParts = request.getFullName().split(" ");
        List<String> firstNameParts = new ArrayList<>();
        for (int i = 0; i < nameParts.length; i++) {
            if (i == nameParts.length - 1) {
                userEntity.lastName = StringUtils.capitalize(nameParts[i]);
            } else {
                firstNameParts.add(StringUtils.capitalize(nameParts[i].trim()));
            }
        }

        userEntity.firstName = String.join( " ", firstNameParts);

        userEntity.status = UserStatus.Pending.value();
        userEntity.password = passwordEncoder.encode(request.getPassword());

        int year = LocalDateTime.now().getYear();
        String yearPad = String.valueOf(year - 2000);
        String monthPad = String.format("%02d", LocalDateTime.now().getMonthValue());
        String dayPad = String.format("%02d", LocalDateTime.now().getDayOfMonth());
        Long sequence = sequenceRepository.createByCodeAndYear(SequenceType.SBD, year);
        String sequencePad = String.format("%04d", sequence);
        userEntity.sbd = yearPad + monthPad + dayPad + sequencePad;
        userEntity.id = userRepository.insert(userEntity);

        userRoleRepository.insert(Collections.singleton(new UserRoleEntity(userEntity.id, Role.User.value())));

        String key = passwordEncoder.encode(UUID.randomUUID().toString());
        UserRegistrationEntity userRegistrationEntity = new UserRegistrationEntity(userEntity.id, RegistrationKey.ACTIVE_KEY, key);
        registrationRepository.insert(userRegistrationEntity);

        return userMapper.toUserResponse(userEntity);
    }

    public void active(@Valid ActiveRequest request) {
        UserRegistrationEntity registration =
                registrationRepository.findByKeyAndValue(RegistrationKey.ACTIVE_KEY, request.getHash());

        if (registration == null) {
            throw new FamException(ErrorInfo.BAD_REQUEST);
        }
        if (RegistrationStatus.activated.equals(registration.status)) {
            throw new FamException(ErrorInfo.USER_ACTIVATED);
        }

        UserEntity user = userRepository.findById(registration.userId);
        if (user == null) {
            throw new FamException(ErrorInfo.USER_NOT_EXISTED);
        }

        user.status = UserStatus.Active.value();

        userRepository.update(user);


        registrationRepository.updateStatus(registration.id, RegistrationStatus.activated);
    }

    public String generateUsername(GenUserNameRequest request) {
        String fullname = Stream.of(request.getFirstName(), request.getLastName())
                .filter(s -> s != null && !s.isEmpty())
                .map(String::toLowerCase)
                .flatMap(p -> Arrays.stream(p.split(" "))).collect(Collectors.joining(" "));

        if (fullname.isEmpty()) {
            fullname = "hs";
        }

        String username = getNameFromText(fullname);

        long start = 1L;
        int page = 0;
        int size = 1000;
        List<UserEntity> existing = userRepository.findUsernameLike(username, size, 0);
        while (!existing.isEmpty()) {
            for (UserEntity e : existing) {
                start = Math.max(start, Long.parseLong(e.username.toLowerCase().replace(username.toLowerCase(), "")));
            }

            page++;
            existing = userRepository.findUsernameLike(username, size, (long) page * size);
        }

        if (start == 1L) {
            return username;
        }

        return username + (start + 1L);
    }

    private String getNameFromText(@NotBlank String text) {
        Assert.notNull(text);
        String[] name = removeVietnameseChars(text).toLowerCase().split(" ");

        String partLast = name[name.length - 1].toLowerCase();
        StringBuilder username = new StringBuilder(partLast.substring(0, 1).toUpperCase() + partLast.substring(1));

        if (name.length > 1) {
            for (int i = 0; i < name.length - 1; i++) {
                username.append(name[i].substring(0, 1).toUpperCase());
            }
        }

        return username.toString();
    }

    private static String removeVietnameseChars(String input) {
        String normalized = Normalizer.normalize(input, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(normalized).replaceAll("");
    }
}
