package net.bestinme.fami.service;

public interface YoutubeService {
    String getTempLink(String videoId, long duration);
}
