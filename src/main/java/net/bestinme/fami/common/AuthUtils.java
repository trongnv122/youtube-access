package net.bestinme.fami.common;

import org.eclipse.microprofile.jwt.JsonWebToken;

public class AuthUtils {
    public static Long getUserId(JsonWebToken jwt) {
        return Long.parseLong(jwt.getClaim("uid").toString());
    }
}
