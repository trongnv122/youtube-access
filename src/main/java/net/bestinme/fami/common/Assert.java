package net.bestinme.fami.common;

import java.util.Objects;

public class Assert {
    /*
   Assert for not null
    */
    public static void notNull(Object o, Object... error) {
        checkAndThrow(o != null, error);
    }

    /*
    Assert for null
     */
    public static void isNull(Object o, Object... error) {
        checkAndThrow(o == null, error);
    }

    /*
    Assert for true
     */
    public static void isTrue(Object o, Object... error) {
        checkAndThrow(Objects.equals(Boolean.TRUE, o), error);
    }

    /*
    Assert for true
     */
    public static void equals(Object o, Object o2, Object... error) {
        checkAndThrow(Objects.equals(o2, o), error);
    }

    private static void checkAndThrow(Object boo, Object... error) {
        if (Boolean.TRUE.equals(boo)) {
            return;
        }
        if (error != null && error.length == 1 && error[0] instanceof FamException) {
            throw (FamException) error[0];
        }
        if (error != null && error.length == 1 && error[0] instanceof ErrorInfo) {
            throw new FamException((ErrorInfo) error[0]);
        }
        if (error != null && error.length == 1 && error[0] instanceof String) {
            throw new FamException(ErrorInfo.UNKNOWN_ERROR.getCode(), (String) error[0]);
        }
        if (error != null && error.length == 2 && error[0] instanceof Integer && error[1] instanceof String) {
            throw new FamException((Integer) error[0], (String) error[1]);
        }
        if (error != null && error.length == 2 && error[0] instanceof String && error[1] instanceof String) {
            throw new FamException(ErrorInfo.badRequest((String) error[0], (String) error[1]));
        }
    }
}
