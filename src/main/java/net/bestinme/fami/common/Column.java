package net.bestinme.fami.common;

public class Column {
    public Column(){}
    public String name;

    public Column(String name, Sort.Direction direction) {
        this.name = name;
        this.direction = direction;
    }

    public Sort.Direction direction;
}
