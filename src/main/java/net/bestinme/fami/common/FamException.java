package net.bestinme.fami.common;


public class FamException extends RuntimeException {
    private final ErrorInfo error;

    public FamException(ErrorInfo error) {
        super();
        this.error = error;
    }

    public FamException(Integer code, String message) {
        super();
        this.error = new ErrorInfo(code, message);
    }

    public FamException(Throwable cause) {
        super(cause);
        this.error = ErrorInfo.UNKNOWN_ERROR;
    }

    public ErrorInfo getError() {
        return error;
    }
}
