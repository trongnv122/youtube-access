package net.bestinme.fami.common;

import java.util.ArrayList;
import java.util.List;

public class Sort {
    public List<Column> columns = new ArrayList<>();

    public static Sort by(String id, Direction descending) {
        Sort sort = new Sort();
        sort.columns.add(new Column(id,descending));
        return sort;
    }

    public enum Direction {
        ASC,
        DESC;
    }

    public int size() {
        return columns.size();
    }

}
