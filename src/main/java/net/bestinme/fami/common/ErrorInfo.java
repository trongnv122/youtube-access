package net.bestinme.fami.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorInfo {
    private static final Logger logger = LoggerFactory.getLogger(ErrorInfo.class);

    @JsonIgnore
    public static Properties properties;
    private int code;
    private String message;

    private List<String> messages = new ArrayList<>();
    private Map<String, String> messageDetails = new HashMap<>();

    static {
        try {
            ClassLoader classLoader = ErrorInfo.class.getClassLoader();
            InputStream input = classLoader.getResourceAsStream("/error_info.properties");
            InputStreamReader inputStreamReader = new InputStreamReader(input, StandardCharsets.UTF_8);
            properties = new Properties();
            properties.load(inputStreamReader);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public static final int BAD_REQUEST_CODE = 400;
    public static final ErrorInfo BAD_REQUEST = new ErrorInfo(BAD_REQUEST_CODE, properties.getProperty("bad.request"));

    public static final int UNKNOWN_ERROR_CODE = 2000;
    public static final ErrorInfo UNKNOWN_ERROR = new ErrorInfo(UNKNOWN_ERROR_CODE, properties.getProperty("server.error"));

    public static final int INVALID_DATA_CODE = 2001;
    public static final ErrorInfo INVALID_DATA = new ErrorInfo(INVALID_DATA_CODE, properties.getProperty("invalid.data"));

    // USER ERROR CODE FROM 3000
    public static final int USER_EXISTED_CODE = 3000;
    public static final ErrorInfo USER_EXISTED = new ErrorInfo(USER_EXISTED_CODE, properties.getProperty("user.existed"));

    public static final int USER_NOT_EXISTED_CODE = 3001;
    public static final ErrorInfo USER_NOT_EXISTED = new ErrorInfo(USER_EXISTED_CODE, properties.getProperty("user.not.existed"));

    public static final int USER_ACTIVATED_CODE = 3002;
    public static final ErrorInfo USER_ACTIVATED = new ErrorInfo(USER_ACTIVATED_CODE, properties.getProperty("user.activated"));

    public ErrorInfo(int code) {
        this.code = code;
        this.message = properties.getProperty("server.error");
    }

    public ErrorInfo(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ErrorInfo(int code, List<String> messages) {
        this.code = code;
        this.messages = messages;
    }

    public ErrorInfo(List<String> messages) {
        this.code = UNKNOWN_ERROR_CODE;
        this.messages = messages;
    }

    public ErrorInfo(int code, Map<String, String> messageDetails) {
        this.code = code;
        this.messageDetails = messageDetails;
    }

    public ErrorInfo(Map<String, String> messageDetails) {
        this.code = UNKNOWN_ERROR_CODE;
        this.messageDetails = messageDetails;
    }

    public static ErrorInfo badRequest(String path, String message) {
        ErrorInfo error = new ErrorInfo(BAD_REQUEST_CODE);
        error.messageDetails.put(path, message);
        return error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public Map<String, String> getMessageDetails() {
        return messageDetails;
    }

    public void setMessageDetails(Map<String, String> messageDetails) {
        this.messageDetails = messageDetails;
    }
}
