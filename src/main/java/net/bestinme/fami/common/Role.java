package net.bestinme.fami.common;

public enum Role {
    Admin(1), User(2), Parent(3), Child(4), Teacher(5);

    private final Integer role;

    Role(Integer role) {
        this.role = role;
    }

    public Integer value() {
        return role;
    }
}
