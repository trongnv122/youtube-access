package net.bestinme.fami.enums;

public enum UserStatus {
    Pending (0),
    Active (1),
    Inactive (2);

    private final Integer value;

    UserStatus(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return value;
    }
}
