package net.bestinme.fami.utils;

import net.bestinme.fami.config.QuarkusJWTConfigs;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

/**
 * @author trongnv
 */
@ApplicationScoped
public class PBKDF2Encoder {
    final QuarkusJWTConfigs jwtConfigs;

    @Inject
    public PBKDF2Encoder(QuarkusJWTConfigs jwtConfigs) {
        this.jwtConfigs = jwtConfigs;
    }

    /**
     * @param cs password
     * @return encoded password
     */
    public String encode(CharSequence cs) {
        try {
            byte[] result = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
                    .generateSecret(new PBEKeySpec(cs.toString().toCharArray(),
                            jwtConfigs.getPasswordSecret().getBytes(),
                            jwtConfigs.getPasswordIteration(),
                            jwtConfigs.getPasswordKeyLength()))
                    .getEncoded();
            return Base64.getEncoder().encodeToString(result);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static void main(String[] args) {
        try {
            byte[] result = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
                    .generateSecret(new PBEKeySpec("".toCharArray(), "".getBytes(), 33, 128))
                    .getEncoded();
            System.out.println(Base64.getEncoder().encodeToString(result));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new RuntimeException(ex);
        }
    }
}
