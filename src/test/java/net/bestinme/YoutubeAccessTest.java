package net.bestinme;

import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import net.bestinme.fami.config.YouTubeConfigs;
import net.bestinme.fami.service.YouTubeDataAPI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.List;

public class YoutubeAccessTest {
    @Test
    public void testList() throws IOException {
        // Mock
        YouTubeConfigs configMock = Mockito.mock(YouTubeConfigs.class);
        Assertions.assertNotNull(System.getenv().get("YOUTUBE_APIKEY"));

        Mockito.when(configMock.getApikey()).thenReturn(System.getenv().get("YOUTUBE_APIKEY"));
        YouTubeDataAPI api = new YouTubeDataAPI(configMock);

        YouTube.Search.List search = api.get().search().list(List.of("snippet"));
        search.setType(List.of("video"));
        SearchListResponse searchResponse = search.setQ("Mit").execute();

        Assertions.assertNotNull(searchResponse);
    }


}
